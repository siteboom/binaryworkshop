export interface User {
  id: number | null,
  name: String,
  email: String
}
